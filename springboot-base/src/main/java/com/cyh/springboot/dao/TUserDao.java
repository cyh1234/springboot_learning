package com.cyh.springboot.dao;

import com.cyh.springboot.entity.TUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cyh
 * @since 2018-08-07
 */
public interface TUserDao extends BaseMapper<TUser> {

}
