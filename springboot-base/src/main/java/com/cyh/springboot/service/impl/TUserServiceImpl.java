package com.cyh.springboot.service.impl;

import com.cyh.springboot.entity.TUser;
import com.cyh.springboot.dao.TUserDao;
import com.cyh.springboot.service.TUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cyh
 * @since 2018-08-07
 */
@Service
public class TUserServiceImpl extends ServiceImpl<TUserDao, TUser> implements TUserService {

}
