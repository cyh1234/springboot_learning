package com.cyh.springboot.service;

import com.cyh.springboot.entity.TUser;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyh
 * @since 2018-08-07
 */
public interface TUserService extends IService<TUser> {

}
