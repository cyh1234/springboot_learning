package com.cyh.springboot.web;

import com.cyh.springboot.entity.TUser;
import com.cyh.springboot.service.TUserService;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class Hello {

    @Autowired
    private TUserService tUserService;

    @RequestMapping("/hello")
    public String  fun(){
        List<TUser> tUsers = tUserService.selectByMap(ImmutableMap.of("name", "a30"));
        return "hello";
    }
}
