quartz的参考教程， 不清楚的同学可以看这里：
https://www.jianshu.com/p/7e755698d58a


---

##  引入依赖
和springboot集成只需要引入这个即可
```
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-quartz</artifactId>
</dependency>
```

## 词汇解释
```
Scheduler – (调度器)与scheduler交互的主要API； 
Job – (作业)你通过scheduler执行任务，你的任务类需要实现的接口； 
JobDetail – (作业实例)定义Job的实例； 
Trigger – (触发器)触发Job的执行； 
JobBuilder – 定义和创建JobDetail实例的接口; 
TriggerBuilder – 定义和创建Trigger实例的接口；
```

## 数据表说明 (只有做集群才会将job信息持久化到数据库，单机选择内存方式就行了)

  Table Name              	Description                         
  QRTZ_CALENDARS          	存储Quartz的Calendar信息                 
  QRTZ_CRON_TRIGGERS      	存储CronTrigger，包括Cron表达式和时区信息        
  QRTZ_FIRED_TRIGGERS     	存储与已触发的Trigger相关的状态信息，以及相联Job的执行信息  
  QRTZ_PAUSED_TRIGGER_GRPS	存储已暂停的Trigger组的信息                   
  QRTZ_SCHEDULER_STATE    	存储少量的有关Scheduler的状态信息，和别的Scheduler实例
  QRTZ_LOCKS              	存储程序的悲观锁的信息                         
  QRTZ_JOB_DETAILS        	存储每一个已配置的Job的详细信息                   
  QRTZ_JOB_LISTENERS      	存储有关已配置的JobListener的信息              
  QRTZ_SIMPLE_TRIGGERS    	存储简单的Trigger，包括重复次数、间隔、以及已触的次数      
  QRTZ_BLOG_TRIGGERS      	Trigger作为Blob类型存储                   
  QRTZ_TRIGGER_LISTENERS  	存储已配置的TriggerListener的信息            
  QRTZ_TRIGGERS           	存储已配置的Trigger的信息                    

暂时只用到了以下表
```
select * from qrtz_job_details;  -- 所有job详情
select * from qrtz_triggers;  -- 所有trigger
select * from qrtz_cron_triggers;  --  CronTrigger信息
```


## 触发器状态（trigger_state）
```
PAUSED暂停
ACQUIRED执行中
WAITING等待
ERROR 错误
```

---
## quartz管理系统
码云地址:https://gitee.com/cyh1234/springboot_learning/tree/master/springboot-job
因为前端用了element，我是第一次用， 这里记录下如何打包
1. 下载安装node.js，并配置环境变量
2.  cd vue-manage-system-master
3. npm install  
4. npm run dev

实现了添加任务， 修改任务， 停止恢复任务， 查看任务， 删除任务的功能。
![输入图片说明](https://images.gitee.com/uploads/images/2018/0813/213645_3cf4bfbb_1367918.png "屏幕截图.png")