import Vue from 'vue';

import Qs from 'qs';

// 使用 Event Bus
const bus = new Vue();

export default bus;