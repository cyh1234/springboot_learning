package com.cyh.job.service;

import com.cyh.job.entity.QuartzEntity;
import com.cyh.job.entity.ReturnPage;
import org.quartz.SchedulerException;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyh
 * @since 2018-08-07
 */
public interface JobService{

    ReturnPage<QuartzEntity> findJobList(QuartzEntity quartzEntity);

    Integer getJobCount();

    void saveOrUpdateJob(QuartzEntity quartzEntity, boolean isAdd) throws SchedulerException, ClassNotFoundException, Exception;

    void delete(QuartzEntity quartzEntity);
}
