package com.cyh.job.dao;


import com.cyh.job.entity.QuartzEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 * @author cyh
 * @since 2018-08-07
 */
public interface JobDao {

    /**
     * 查询job列表
     * @param page
     * @param quartzEntity
     * @return
     */
    List<QuartzEntity> findJobList(@Param("dto") QuartzEntity quartzEntity);

    /**
     * 查询总数
     * @param page
     * @param quartzEntity
     * @return
     */
    Integer getTotal(@Param("dto") QuartzEntity quartzEntity);
}
