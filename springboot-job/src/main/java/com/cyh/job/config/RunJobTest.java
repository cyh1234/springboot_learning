package com.cyh.job.config;

import com.cyh.job.dao.JobDao;
import com.cyh.job.entity.QuartzEntity;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 步骤：
 *  1. 得到scheduler实例
 *  2. 创建jobDetail (jobname, jobgroup jobclass, )
 *  3. 创建Trigger触发器
 *  4. 绑定job和trigger
 *
 */


@Component
public class RunJobTest implements ApplicationRunner {
    private static final Logger logger = LoggerFactory.getLogger(RunJobTest.class);

    @Autowired
    private Scheduler scheduler;

    /**
     * 启动时执行此方法
     * @param args
     * @throws Exception
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {

        //判断是否加入过此测试job， 加入过则不加入
//        logger.info("----------------------------初始化一个测试job");
//        QuartzEntity quartzEntity = new QuartzEntity();
//        quartzEntity.setJobName("testJob1");
//        quartzEntity.setCronExpression("0/2 * * * * ?");
//        quartzEntity.setJobClassName("com.cyh.job.job.TestJob");
//        quartzEntity.setJobDesc("这是一个测试的任务");
//        quartzEntity.setJobGroup("testJob");
//        Class cls = Class.forName("com.cyh.job.job.TestJob");
//        //设置job详情
//        JobDetail detail = JobBuilder.newJob(cls).withIdentity(quartzEntity.getJobName(), quartzEntity.getJobGroup())
//                .withDescription(quartzEntity.getJobDesc())
//                .usingJobData("data", "我的测试job").build();
//        //设置触发器
//        CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(quartzEntity.getCronExpression());
//        Trigger trigger = TriggerBuilder.newTrigger().
//                withIdentity(quartzEntity.getJobName(), quartzEntity.getJobGroup())
//                .startNow().withSchedule(cronScheduleBuilder).build();
//        scheduler.scheduleJob(detail, trigger);
    }
}
