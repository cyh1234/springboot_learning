package com.cyh.job.utils;

import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CyhDateUtil {

    public static String NYRSFM = "yyyy-MM-dd HH:mm:ss";

    public static String timeStampToString(String time, String pattern) {
        Date date = new Date();
        date.setTime(Long.parseLong(time));
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        String format = sdf.format(date);
        return format;
    }
}
