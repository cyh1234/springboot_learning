package com.cyh.job.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回给前端
 */
public class Result extends HashMap<String, Object> {

	private static final long serialVersionUID = 1L;

	public static Result success(Object obj) {
		Result r = new Result();
		r.put("data", obj);
		r.put("code","1");
		return r;
	}

	public static Result fail(Object obj) {
		Result r = new Result();
		r.put("data", obj);
		r.put("code", "-1");
		return r;
	}
}