package com.cyh.job.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.cyh.job.utils.CyhDateUtil;
import com.cyh.job.utils.CyhStringUtil;
import freemarker.template.utility.StringUtil;
import org.springframework.util.StringUtils;

public class QuartzEntity extends BaseEntity {
    private String jobName;
    private String jobGroup;
    private String description;
    private String jobClassName;
    private String cronExpression;
    private String nextTime;
    private String prevTime;
    private String triggerName;
    private String triggerGroup;
    private String triggerState;

    //前端修改job时，会带入旧的job名称和组
    private String oldJobName;
    private String oldJobGroup;

    public QuartzEntity() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobGroup() {
        return jobGroup;
    }

    public void setJobGroup(String jobGroup) {
        this.jobGroup = jobGroup;
    }


    public String getJobClassName() {
        return jobClassName;
    }

    public void setJobClassName(String jobClassName) {
        this.jobClassName = jobClassName;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getTriggerName() {
        return triggerName;
    }

    public void setTriggerName(String triggerName) {
        this.triggerName = triggerName;
    }

    public String getTriggerState() {
        return triggerState;
    }

    public void setTriggerState(String triggerState) {
        this.triggerState = triggerState;
    }

    public String getOldJobName() {
        return oldJobName;
    }

    public void setOldJobName(String oldJobName) {
        this.oldJobName = oldJobName;
    }

    public String getOldJobGroup() {
        return oldJobGroup;
    }

    public void setOldJobGroup(String oldJobGroup) {
        this.oldJobGroup = oldJobGroup;
    }

    public String getTriggerGroup() {
        return triggerGroup;
    }

    public void setTriggerGroup(String triggerGroup) {
        this.triggerGroup = triggerGroup;
    }

    public String getNextTime() {
        if (CyhStringUtil.isNotEmpty(this.nextTime)) {
            return CyhDateUtil.timeStampToString(this.nextTime, CyhDateUtil.NYRSFM);
        }
        return nextTime;
    }

    public void setNextTime(String nextTime) {
        this.nextTime = nextTime;
    }

    public String getPrevTime() {
        if (CyhStringUtil.isNotEmpty(this.prevTime)) {
            return CyhDateUtil.timeStampToString(this.prevTime, CyhDateUtil.NYRSFM);
        }
        return prevTime;
    }

    public void setPrevTime(String prevTime) {
        this.prevTime = prevTime;
    }
}
