package com.cyh.job.entity;

public class BaseEntity {
    //访问第几页
    private Integer page;
    //每页显示多少条
    private Integer pageSize = 10;

    public Integer getPage() {
        if (page != null){
            return (page -1) * pageSize;
        }
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
