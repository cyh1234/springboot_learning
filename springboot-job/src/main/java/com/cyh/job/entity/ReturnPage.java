package com.cyh.job.entity;

import com.cyh.job.utils.Result;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public class ReturnPage<T> {
    //总数量
    private Integer total;
    //返回列表结果
    private List<T> data;

    public static<T> ReturnPage<T> create(Integer total, List<T> data){
        ReturnPage<T> page = new ReturnPage<>();
        page.setTotal(total);
        page.setData(data);
        return page;
    }
    public static<T> ReturnPage<T> empty(){
        ReturnPage<T> page = new ReturnPage<>();
        page.setTotal(0);
        page.setData(null);
        return page;
    }
    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
